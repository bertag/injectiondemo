Procedure
----------

1. Created new Maven project using the http://mvnrepository.com/artifact/org.codehaus.mojo.archetypes/webapp-javaee7/1.1 Maven Archetype
2. Set up `.gitignore` to ignore Eclipse-specific project metadata
3. Created `TestInjectable` and annotated it as a `@Singleton` EJB
4. Created `TestService` and annotated it as a `@Stateless` EJB.  `TestService` requires a `TestInjectable` to be injected via the `@Inject` annotation.
5. Created `MyApplication` to resolve the web service root path.
6. Deployed to Glassfish with no additional configuration.  The injections work properly.