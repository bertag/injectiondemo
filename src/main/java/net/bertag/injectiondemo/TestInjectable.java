package net.bertag.injectiondemo;

import javax.ejb.Singleton;

@Singleton
public class TestInjectable {
	
	private String var1 = "one";
	private String var2 = "two";

	public String getVar1() {
		return var1;
	}

	public String getVar2() {
		return var2;
	}

}
