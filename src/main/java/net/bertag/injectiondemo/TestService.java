package net.bertag.injectiondemo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Stateless
@Path("/")
public class TestService {
	
	private TestInjectable injectable;
	
	@Inject
	public TestService(TestInjectable injectable) {
		this.injectable = injectable;
	}
	
	@GET
	public Response displayResponse() {
		return Response.ok("var1: " + injectable.getVar1() + "\nvar2: " + injectable.getVar2()).build();
	}

}
