package net.bertag.injectiondemo;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * This class is merely a wrapper for the application server.  It does nothing.
 * 
 * @author Scott Bertagnole <scott_bertagnole@byu.edu>
 */
@ApplicationPath("/")
public class MyApplication extends Application {

}
